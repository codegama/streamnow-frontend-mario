import { Component , AfterViewInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

declare var $: any ;

@Component({
    templateUrl: 'forgot-password.component.html',
    styleUrls:[
        '../../../../assets/css/bootstrap/css/bootstrap.min.css',
        '../../../../assets/css/font-awesome/css/font-awesome.min.css',
        '../../../../assets/css/mdb.css',
        '../../../../assets/css/style.css',
        '../../../../assets/css/responsive.css',
    ]
})

export class ForgotPasswordComponent{
    ngOnInit(){
        var height = window.innerHeight;
        var content_height = height - 70;
        $('.body-img').height(content_height);
        $('.overlay').height(content_height);
    }

    uType : string; //The user is streamer / viewer

    errorMessages : string;

    constructor(private route:ActivatedRoute, private userService : UserService, private router : Router, public translate : TranslateService) {

        this.route.queryParams.subscribe(params => {

            this.uType = params['uType'];

        });

        if (this.uType == '' || this.uType == null || this.uType == undefined) {

            this.uType = 'viewer';

        }

        this.errorMessages = "";


    }
    

    sendResetPassword(email) {

        this.userService.forgotPassword(email)
            .subscribe(

                (data : any) => {

                    if (data.success == true) {

                        $.toast({
                            heading: 'Success',
                            text: this.translate.instant('forgot_password_success'),
                        // icon: 'error',
                            position: 'top-right',
                            stack: false,
                            textAlign: 'left',
                            loader : false,
                            showHideTransition: 'slide'
                        });

                        this.router.navigate(['/login'], {queryParams : {uType : this.uType}});

                    } else {

                        this.errorMessages = data.error_messages;

                        $.toast({
                            heading: 'Error',
                            text: this.errorMessages,
                           // icon: 'error',
                            position: 'top-right',
                            stack: false,
                            textAlign: 'left',
                            loader : false,
                            showHideTransition: 'slide'
                        });
                        
                    }

                },

                (err : HttpErrorResponse) => {

                    this.errorMessages = this.translate.instant('something_went_wrong');

                    $.toast({
                        heading: 'Error',
                        text: this.errorMessages,
                       // icon: 'error',
                        position: 'top-right',
                        stack: false,
                        textAlign: 'left',
                        loader : false,
                        showHideTransition: 'slide'
                    });

                }
            );

    }
}