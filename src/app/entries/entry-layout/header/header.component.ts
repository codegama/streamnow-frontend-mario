import { Component, ElementRef, AfterViewInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RequestService } from '../../../common/services/request.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AppService } from '../../../app.service';
import { Constant } from '../../../constant/Constant';

declare var $: any

@Component({
    selector: 'common-header',
    templateUrl: 'header.component.html',
    styleUrls:[
        '../../../../assets/css/bootstrap/css/bootstrap.min.css',
        '../../../../assets/css/font-awesome/css/font-awesome.min.css',
        '../../../../assets/css/style.css',
        '../../../../assets/css/responsive.css',
    ],
    host: {
        '(document:click)': 'onClick($event)',
    },
})

export class EntryHeaderComponent implements AfterViewInit,OnDestroy{
    public elementRef;

    searchUsers : any[];

    searchLiveTV : any[];

    searchVideos : any[];

    site_settings : any;

    site_logo : string;

    notifications : any[];

    errorMessages : string;

    notification_count : number;

    bellNotificationStatus : any;

    userId : any;

    key_term :string;

    notifications_types : any;

    constructor(private appService : AppService, public  router : Router, myElement: ElementRef, private requestService : RequestService) {      

        this.elementRef = myElement;

        this.searchUsers = [];

        this.searchLiveTV = [];

        this.searchVideos = [];

        this.notifications = [];

        this.errorMessages = "";

        this.notifications_types = {

            'LIVE_STREAM_STARTED' : Constant.LIVE_STREAM_STARTED,

            'USER_FOLLOW' : Constant.USER_FOLLOW,

            'USER_JOIN_VIDEO' : Constant.USER_JOIN_VIDEO,

            'USER_GROUP_ADD' : Constant.USER_GROUP_ADD

        };

        this.site_settings = this.appService.appDetails();

        this.userId = (localStorage.getItem('userId') != '' && localStorage.getItem('userId') != null && localStorage.getItem('userId') != undefined) ? localStorage.getItem('userId') : '';

        if (this.userId) {

            this.bellNotifications();

        }

        this.notification_count = 0;
    }

    ngAfterViewInit(){
        // $.getScript('../../../../assets/js/jquery.min.js',function(){
        //     console.log('jqury.min');
        // });
        // $.getScript('../../../../assets/js/script.js',function(){
        //     console.log('script');
        // });
        // $.getScript('../../../../assets/js/jquery-ui.js',function(){
        //     console.log('script-js');
        // });

        this.site_settings = JSON.parse(localStorage.getItem('site_settings'));

        let site_logo = (this.site_settings).filter(obj => {
            return obj.key === 'site_logo'
        });

        setTimeout(()=>{

            this.site_logo = site_logo.length > 0 ? site_logo[0].value : '';

        }, 1000);

        if (this.userId) {

            this.bellNotificationStatus = setInterval(()=>{

                this.bellNotificationsCount();

            }, 10 * 1000);

        }

        
    }


    resizeContent(){
        $("#full-view").toggleClass("width100");
	    $("#full-view").toggleClass("left-right-padding");
	    $("#side-view").toggleClass("zero-width");
    }

    ngOnDestroy() {

        clearInterval(this.bellNotificationStatus);

    }

    onSearchChange(searchValue : string ) {  
        
        this.key_term = searchValue;

        this.requestService.postMethod("search", {term : searchValue , skip : 0})
        .subscribe(

            (data : any) =>  {

                if(data.success) {

                    this.searchUsers = data.data.users.data;

                    this.searchLiveTV = data.data['live-tv'].data;

                    this.searchVideos = data.data['live-videos'].data;

                } else {

                    this.searchUsers = [];

                    this.searchLiveTV = [];

                    this.searchVideos = [];
                    
                }

            },

            (err : HttpErrorResponse) => {

                console.log("Search! Oops something went wrong..!");

            }
            
        );

    }

    onKeyEsc(event) {

        if (event.keyCode === 27) {

            this.searchUsers = [];

            this.searchLiveTV = [];

            this.searchVideos = [];

            $("#search_results").val("");

        }
    }
  
    onClick(event) {

        this.searchUsers = [];

        this.searchLiveTV = [];

        this.searchVideos = [];

        $("#search_results").val("");
        
    }

    onKeyEnter(value) {

        this.searchUsers = [];

        this.searchLiveTV = [];

        this.searchVideos = [];

        $("#search_results").val("");

        this.router.navigate(['/search'], {queryParams : {term : value}});

    }

    bellNotifications() {


        this.requestService.postMethod("user/notifications", {skip : 0}) 
        .subscribe(

            (data : any) => {

                if (data.success == true) {
                    
                    this.notifications = data.data;

                } else {

                    this.errorMessages = data.error_messages;

                    $.toast({
                        heading: 'Error',
                        text: this.errorMessages,
                    // icon: 'error',
                        position: 'top-right',
                        stack: false,
                        textAlign: 'left',
                        loader : false,
                        showHideTransition: 'slide'
                    });
                    
                }

            },

            (err : HttpErrorResponse) => {

                this.errorMessages = 'Oops! Something Went Wrong';

                $.toast({
                    heading: 'Error',
                    text: this.errorMessages,
                // icon: 'error',
                    position: 'top-right',
                    stack: false,
                    textAlign: 'left',
                    loader : false,
                    showHideTransition: 'slide'
                });

            }
        );

    }

    bellNotificationsCount() {

        this.requestService.postMethod("get/notification/count", {}) 
        .subscribe(

            (data : any) => {

                if (data.success == true) {
                    
                    this.notification_count = data.count;

                } else {

                    this.errorMessages = data.error_messages;

                    $.toast({
                        heading: 'Error',
                        text: this.errorMessages,
                    // icon: 'error',
                        position: 'top-right',
                        stack: false,
                        textAlign: 'left',
                        loader : false,
                        showHideTransition: 'slide'
                    });
                    
                }

            },

            (err : HttpErrorResponse) => {

                this.errorMessages = 'Oops! Something Went Wrong';

                $.toast({
                    heading: 'Error',
                    text: this.errorMessages,
                // icon: 'error',
                    position: 'top-right',
                    stack: false,
                    textAlign: 'left',
                    loader : false,
                    showHideTransition: 'slide'
                });

            }
        );

    }

    notificationStatusChange() {

        if(this.notification_count > 0) {

            this.requestService.postMethod("status/notifications", {}) 
            .subscribe(
    
                (data : any) => {
    
                    this.notification_count = 0;

                    this.notifications = [...data.notifications, ...this.notifications];

                },
    
                (err : HttpErrorResponse) => {
    
                    this.errorMessages = 'Oops! Something Went Wrong';
    
                    $.toast({
                        heading: 'Error',
                        text: this.errorMessages,
                    // icon: 'error',
                        position: 'top-right',
                        stack: false,
                        textAlign: 'left',
                        loader : false,
                        showHideTransition: 'slide'
                    });
    
                }
            );

        } else {

            console.log("Notification count "+this.notification_count);

        }

    }
}