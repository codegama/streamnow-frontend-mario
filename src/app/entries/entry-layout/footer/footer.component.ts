import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../../common/services/request.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Admin } from '../../../models/admin';

declare var $:any;

@Component({
    selector: 'common-footer',
    templateUrl: 'footer.component.html',
    styleUrls:[
        '../../../../assets/css/bootstrap/css/bootstrap.min.css',
        '../../../../assets/css/font-awesome/css/font-awesome.min.css',
        '../../../../assets/css/style.css',
        '../../../../assets/css/responsive.css',
    ],
})

export class EntryFooterComponent implements OnInit{

    errorMessages : string;

    pages_list : any[];

    admin : Admin;

    site_settings : any[];

    twitter_link : string;
    
    facebook_link : string;

    linkedin_link : string;

    google_plus_link : string;

    pinterest_link : string;

    constructor(private requestService : RequestService, private router : Router) {

        this.errorMessages = '';

        this.pages_list = [];

        this.admin = {};

        this.site_settings = JSON.parse(localStorage.getItem('site_settings'));
    }


    ngOnInit(){
        var footer_height = $('#footer_sec').outerHeight();

        // As of now @TODO

        footer_height = parseFloat(footer_height) + 60;

        $('.bottomheight').height(footer_height);

        this.pagesListFn('pages/list', "");

        this.adminDetailFn('admin', "");

        let twitter_link = (this.site_settings).filter(obj => {
            return obj.key === 'twitter_link'
        });
        let facebook_link = (this.site_settings).filter(obj => {
            return obj.key === 'facebook_link'
        });
        let linkedin_link = (this.site_settings).filter(obj => {
            return obj.key === 'linkedin_link'
        });
        let google_plus_link = (this.site_settings).filter(obj => {
            return obj.key === 'google_plus_link'
        });
        let pinterest_link = (this.site_settings).filter(obj => {
            return obj.key === 'pinterest_link'
        });

        this.pinterest_link = pinterest_link.length > 0 ? pinterest_link[0].value : '';

        this.twitter_link = twitter_link.length > 0 ? twitter_link[0].value : '';

        this.facebook_link = facebook_link.length > 0 ? facebook_link[0].value : '';

        this.linkedin_link = linkedin_link.length > 0 ? linkedin_link[0].value : '';

        this.google_plus_link = google_plus_link.length > 0 ? google_plus_link[0].value : '';
    }

    pagesListFn(url, object) {

        this.requestService.postMethod(url,object) 
            .subscribe(

                (data : any) => {

                    if (data.success == true) {

                        this.pages_list = data.data;   

                    } else {

                        this.errorMessages = data.error_messages;

                        $.toast({
                            heading: 'Error',
                            text: this.errorMessages,
                        // icon: 'error',
                            position: 'top-right',
                            stack: false,
                            textAlign: 'left',
                            loader : false,
                            showHideTransition: 'slide'
                        });
                        
                    }

                },

                (err : HttpErrorResponse) => {

                    this.errorMessages = 'Oops! Something Went Wrong';

                    $.toast({
                        heading: 'Error',
                        text: this.errorMessages,
                    // icon: 'error',
                        position: 'top-right',
                        stack: false,
                        textAlign: 'left',
                        loader : false,
                        showHideTransition: 'slide'
                    });

                }

            );
    }

    adminDetailFn(url, object) {

        this.requestService.postMethod(url,object) 
            .subscribe(

                (data : any) => {

                    this.admin = data;   

                },

                (err : HttpErrorResponse) => {

                    this.errorMessages = 'Oops! Something Went Wrong';

                    $.toast({
                        heading: 'Error',
                        text: this.errorMessages,
                    // icon: 'error',
                        position: 'top-right',
                        stack: false,
                        textAlign: 'left',
                        loader : false,
                        showHideTransition: 'slide'
                    });

                }

            );
    }
}