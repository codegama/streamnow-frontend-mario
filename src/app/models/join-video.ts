export interface JoinVideo {
  title: string;
  type: string;
  amount: number;
  description: string;
  viewer_cnt: string;
  name: string;
  snapshot: string;
  is_streaming: number;
  live_group_id: number;
}
